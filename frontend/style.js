export const colors = {
    blackBean: "#403330",
    grapefruit: "#de1b50",
    guacamole: "#b5ef60",
    mustard: "#FFC602",
    jasmineRice: '#f2eee1',
    peach: '#deb5aa',
    salsa: '#e10025',
};

export const layoutStyle = {
    backgroundColor: colors.jasmineRice,
    color: colors.blackBean,
    fontFamily: 'Gotham,Sofia,Courier,Arial,sans-serif',
    height: 'auto',
    marginTop: '40px',
    minWidth: '20em',
    padding: 40,
    opacity: 0.85,
    position: 'absolute',
    textTransform: 'capitalize',
    top: 0,
};

export const video = {
    height: '100%',
    margin: 0,
    objectFit: 'cover',
    padding: 0,
    width: '100%',
};

export const videoBg = {
    height: '100%',
    margin: 0,
    padding: 0,
    width: '100%',
    zIndex: -1,
};

export const linkStyle = {
    color: colors.blackBean,
    marginRight: 15,
    textDecoration: 'none',
};

export const pageName = {
    color: colors.guacamole,
    fontFamily: 'Surveyor,Bodoni',
    fontSize: 24,
    paddingBottom: 10,
};

export const button = {
    color: colors.grapefruit,
    border: 'solid',
    borderColor: colors.blackBean,
    borderRadius: 6,
    borderWidth: 1,
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    fontWeight: 600,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    width: 140,
};

export const footer = {
    borderTopColor: colors.blackBean,
    borderTopStyle: 'solid',
    borderTopWidth: '1px',
    display: 'flex',
    flexDirection: 'row',
    fontFamily: 'Gotham,Sofia,Courier,Arial',
    fontSize: 16,
    justifyContent: 'space-between',
    marginTop: 40,
    paddingTop: 10,
    textDecoration: 'none',
    textTransform: 'uppercase',
    width: '100%',
};

export const subHead = {
    fontFamily: 'Circular,Gotham,Ogonek-Light',
    fontSize: 20,
    paddingBottom: 10,
};

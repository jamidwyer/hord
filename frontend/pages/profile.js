import Link from 'next/link';
import Layout from '../components/Layout';
import {linkStyle} from '../style.js';
  
export default function Profile() {
    return (
      <Layout>
        <h1>Profile</h1>
        <input name="Name" placeholder="What's your name?" />
        <input name="Refrigerator" placeholder="Do you have a refrigerator?"/>
        <input name="timespan" placeholder="How long do you need your hoard to last?"/>
        <Link href="/inner-horde">
            <a style={linkStyle}>Inner Horde</a>
        </Link>
        <Link href="/outer-horde">
            <a style={linkStyle}>Outer Horde</a>
        </Link>
      </Layout>
    );
  }
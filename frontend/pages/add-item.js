import Link from 'next/link';
import Layout from '../components/Layout';
import {linkStyle} from '../style.js';

export default function AddItem() {
  return (
    <Layout>
      <h1>Add Item</h1>
      <input name="Name" placeholder="Item Name" />
      <input name="Purchase Date" placeholder="Purchase Date" />
      <Link href="/hoard">
        <a style={linkStyle}>Hoard</a>
      </Link>
    </Layout>
  );
}
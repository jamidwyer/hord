import useSWR from 'swr';
import { useRouter } from 'next/router';
import Link from 'next/link';
import {linkStyle} from '../../style.js';
import Layout from '../../components/Layout';
import RecipeLink from '../../components/RecipeLink';
import Directions from '../../components/Directions';

function fetcher(url) {
  return fetch(url).then(r => r.json().then(res => {
    console.log(res);
    return res;
  }));
}

export default function Recipe() {
  const router = useRouter();

  const { data, error } = useSWR(`https://api.hord.dev/recipes?name=${router.query.name}`, fetcher);
  if (error) return <div>failed to load</div>
  if (!data) return <div>loading...</div>
  if (!data[0]) return <div>recipe not found</div>
  const {ingredients, directions, link} = data[0];
  return (
    <Layout>
    <h1>{router.query.name}</h1>
      <h2>Ingredients</h2>
      <ul>{ingredients.map(ingredient => {
        return (
          <li key={ingredient.name}>
            <Link href="/item/[name]" as={`/item/${ingredient.name.replace(/\s+/g, '-').toLowerCase()}`}>
              <a style={linkStyle}>{ingredient.name}</a>
            </Link>
          </li>
        );
      })}</ul>
      {directions ? <Directions directions={directions} /> : <RecipeLink link={link}/>}
    </Layout>
  );
}

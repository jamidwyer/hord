import useSWR from 'swr';
import { useRouter } from 'next/router';
import Link from 'next/link';
import {linkStyle} from '../../style.js';
import Layout from '../../components/Layout';

function fetcher(url) {
  return fetch(url).then(r => r.json().then(res => {
    return res;
  }));
}

export default function Item() {
  const router = useRouter();

  const { data, error } = useSWR(`/api/recipes?ingredient=${router.query.name}`, fetcher);
  //const { data, error } = useSWR(`/api/recipes?ingredient=${router.query.name}`, fetcher);
  if (error) return <div>failed to load</div>
  if (!data) return <div>loading...</div>

  const videoSrc = `/images/${router.query.name}.mp4`;

  return (
    <Layout videoSrc={videoSrc}>
    <h1>{router.query.name.replace(/-+/g, " ")}</h1>
    <h2>Recipes</h2>
      <ul>{data.map(recipe => (
        <li key={recipe.name}>
          <Link href="/recipe/[name]" as={`/recipe/${recipe.name}`}>
            <a style={linkStyle}>{recipe.name}</a>
          </Link>
        </li>
        ))}</ul>
    </Layout>
  );
}

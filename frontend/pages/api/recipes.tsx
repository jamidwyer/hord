import { NextApiRequest, NextApiResponse } from 'next';

import allRecipes from '../../data/recipes.json';

type Recipes = ({
  name: string;
  directions: string;
  ingredients: string[];
  link?: undefined;
} | {
  name: string;
  directions: string;
  ingredients: string[];
  link: string;
})[];

export default (req: NextApiRequest, res: NextApiResponse<Recipes>) => {
  const { ingredient, name } = req.query;
  console.log("ingredient is", ingredient);
  let recipes = allRecipes;
  if (ingredient) {
    recipes = recipes.filter(recipe => recipe.ingredients.indexOf(ingredient) > -1); 
  }
  if (name) {
    recipes = recipes.filter(recipe => recipe.name === name); 
  }

  const filteredRecipes = recipes;
  console.log("sending", filteredRecipes);
  res.status(200).json(filteredRecipes);
};

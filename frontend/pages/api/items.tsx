import { NextApiRequest, NextApiResponse } from 'next';

import items from '../../data/items.json';

type Items = ({
  name: string;
  in_stock: boolean;
  expiration_date: string;
  recipes: string[];
} | {
  name: string;
  expiration_date: string;
  recipes: string[];
  in_stock?: undefined;
})[];

export default (req: NextApiRequest, res: NextApiResponse<Items>) => {
  const sortedItems = items.sort(function(a, b) {
    let comparison = 0;
    if (a.expiration_date > b.expiration_date) {
      comparison = 1;
    }
    else if (a.expiration_date < b.expiration_date) {
      comparison = -1;
    }
    return comparison;
  });
  res.status(200).json(sortedItems);
};

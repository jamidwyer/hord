import Link from 'next/link';
import { NextPage } from 'next';
import {linkStyle} from '../style.js';
import Layout from '../components/Layout';
//import items from '../data/items.json';
import fetch from 'isomorphic-unfetch';

interface Props {
  items: any; 
};

const EatNext: NextPage<Props> = props => {
  // TODO: backend should do this with a param passed to say what to sort by
  const sortedItems = props.items.sort(function(a, b) {
    let comparison = 0;
    if (a.expiration_date > b.expiration_date) {
      comparison = 1;
    }
    else if (a.expiration_date < b.expiration_date) {
      comparison = -1;
    }
    return comparison;
  });
  
  return (
    <Layout>
      <h1>Eat Next</h1>
      <ul>
        {sortedItems.map(item => (
          <li key={item.name}>
            <Link href="/item/[name]" as={`/item/${item.name.replace(/\s+/g, "-").toLowerCase()}`}>
              <a style={linkStyle}>{item.name}</a>
            </Link>
          </li>
        ))}
      </ul>
    </Layout>
  );
}

EatNext.getInitialProps = async function() {
  const res = await fetch('https://api.hord.dev/items');
  const data = await res.json();

  console.log(`Show data fetched. Count: ${data.length}`);

  return {
    items: data.map(item => item)
  };
};

export default EatNext;
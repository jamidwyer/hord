import {linkStyle} from '../style.js';

const RecipeLink = (props) => {
return (
  <h2>
    <a style={linkStyle} href={props.link}>
      Recipe
    </a>
  </h2>
);
}

export default RecipeLink;
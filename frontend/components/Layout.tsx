import Head from 'next/head';
import Footer from './Footer';
import {colors, layoutStyle, video, videoBg} from '../style.js';
import { stringify } from 'querystring';

const Layout = props => (
  <>
    <Head>
      <title>Hord</title>
      <meta property="og:title" content="Hord" key="title" />
    </Head>
    <div style={videoBg}>
      <video id="background-video" loop autoPlay muted style={video}>
        <source src={props.videoSrc} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
    </div>
    <div style={layoutStyle}>
      {props.children}
      <Footer />
    </div>
  <style jsx global>{`
        @font-face {
          font-family: 'Gotham';
          src: url('/static/fonts/Gotham/GothamThin.ttf');
          src: url('/static/fonts/Gotham/GothamBold.ttf');
          src: url('/static/fonts/Gotham/GothamSemiBold.ttf');
          src: url('/static/fonts/Gotham/GothamLight.ttf');
        }
        @font-face {
          font-family: 'Bodoni';
          src: url('/static/fonts/Bodoni/Bodoni-1.ttf');
          src: url('/static/fonts/Bodoni/Bodoni-b.ttf');
          src: url('/static/fonts/Bodoni/Bodoni-n.ttf');
        }
        html, body, #__next {
          height: 100%;
          margin: 0;
          padding: 0;
        }
        #__next {
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        h1 {
          color: #de1b50;
          font-family: 'Surveyor,Bodoni';
        }
        a:active, a:hover {
          color: #de1b50;
        }
        a:visited {
          color: #403330;
        }
      `}</style>
        </>
);

export default Layout;
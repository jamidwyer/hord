import {Component} from 'react';
import {colors} from '../style.js';

class Canvas extends Component {
  
componentDidMount() {
    const canvas = this.refs.canvas;
    const ctx = canvas.getContext("2d");
    const img = this.refs.image;
    img.onload = () => {
        ctx.drawImage(img, 0,0);
        ctx.fillStyle = colors.jasmineRice;
    };
}

render() {
    const { width, height, imageToShow } = this.props;

    return (
        <>
            <div>
                <img ref="image" src={imageToShow} />
            </div>
            <div>
                <canvas ref="canvas" width={width} height={height} />
            </div>
        </>    
        );
  }
};

export default Canvas;
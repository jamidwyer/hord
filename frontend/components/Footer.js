import Link from 'next/link';
import {footer, linkStyle} from '../style.js';

const Footer = () => (
  <div style={footer}>
    <Link href="/">
      <a style={linkStyle}>Home</a>
    </Link>
    <a href="https://api.hord.dev/admin/plugins/content-manager/collectionType/application::item.item" style={linkStyle}>Add Item</a>
    <Link href="/eat-next">
      <a style={linkStyle}>Eat Next</a>
    </Link>
    <Link href="/profile">
      <a style={linkStyle}>Profile</a>
    </Link>
  </div>
);

export default Footer;
import React, { PropTypes } from 'react';

import Fragment from './Fragment';
import { colors } from '../style';

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const renderFragments = () => {
    const fragmentCount = getRandomInt(120, 240);
    let fragments = [];
    let i = 0;
    for (i=0; i < fragmentCount; i++) {
        fragments.push(<Fragment key={i} />);
    }
    return fragments;
};

const Terrazzo = () => {
    return (
    <svg width={800}
        height={500}
        style={{background: colors.jasmineRice}}>

        <g>{renderFragments()}
    </g>

    </svg>
    );
}
 
export default Terrazzo;
const Directions = (props) => (
  <div>
      <h2>Directions</h2>
      {props.directions}
  </div>
);

export default Directions;
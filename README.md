# Hord
Currently a work in progress, this app aims to allow people to share resources (their hoard) with their community (their horde).

Right now, it allows one user (me) to add ingredients and use the Eat Next functionality to choose a recipe based on what's gonna go bad next.

Down the road, I'd like people to be able to say, "I need this to last three months." Any excess can be shared with circles of decreasing levels of trust. For example, your inner circle would be close friends and family, and your outer circle might be everyone in your neighborhood, everyone in your city, or everyone.


## Development
`cd frontend`

`nvm use`

`npm run dev`

Load http://localhost:3000.

This will use recipes from the remote database. To run the CMS locally:

`cd cms`

`docker-compose up`

Load http://0.0.0.0:1337.

## Troubleshooting
You may have to sudo for docker-compose up.

## TODO
MVP: what to eat next based on expiration date
go through login process
https://strapi.io/blog/strapi-next-authentication
run prod instead of dev in prod!
faster computer
different users have different recipes
different users have different ingredients
ingredients have different expiration dates for different users
ingredients have different quantities for different users

style strapi

strapize recipes by ingredient (or diwd)
re-deploy next
per-user items
per-user recipes

MVP: better ingredient management

MVP: resource sharing
how long do you need your hord to last
how many people in your inner hord

MVP: outer hord -- people you will share with

Improvements
allow users to have different ingredients and steps for the same recipe, i.e. spaghetti my way vs. theirs
terrazzo bg
hyphens in urls
add url content type
dockerize frontend
deploy strapi
https://strapi.io/documentation/3.0.0-beta.x/guides/deployment.html#amazon-aws
try this: https://github.com/jamidwyer/strapi-nextjs-starter
add content types and content
change master to main
barcode
purchase date for produce
refrigerator yes or no
offline mode
front end vs backend folders
postgres instead of mysql
typescript

## Setup
Docker might suggest you change the version to 2. You can try that, or do this: https://docs.docker.com/install/linux/docker-ce/ubuntu/
https://strapi.io/documentation/3.0.0-beta.x/guides/deployment.html#amazon-aws